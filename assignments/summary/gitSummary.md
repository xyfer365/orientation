# Git Basics
* Git is version-control software that enables us to  collaborate over the web on a project and keep a record of the developmental stages.

## Terminologies

* **Repository**: It is common space where everything related to a project is stored by every team member.
* **Commit**:  It is method to save our work in our local repository.
* **Push**:  It is basically uploading our local repository onto internet.
* **Branch**: Suppose we are working on a system having many subsystems, we create a branch to work specifcally on a particular subsystem.

* **Merge**: When a Branch is free of errors and bugs, we merge it with the main project.

* **Clone**: Cloning is making an exact copy of a GitLab account in our local system.

* **Fork**: Forking is basically copying only the repository under our own username.



### Git Internals
Suppose we are working on a file in a repository. There are three states which a file can have:


* **Modified**: It means that some change is made in that file, but it is not committed to the repository yet.

* **Staged**: The file is marked internally so that when a commit is made, all the staged (marked) files are committed together.

* **Committed**: The file is saved in the local repository.

There are 4 trees into which a file can reside:


* **Workspace**: A tree where a file is modified.

* **Staging**: A tree where all the files marked for committing are stored.

* **Local Repository**: A tree in our local system where all the committed files are stored.

* **Remote Repository**: A tree on the internet where all the local repositories from diffeent individuals are stored.


## Git Cheat Sheet
1. **Git configuration**

![1](/assignments/git_images/1.png)

2. **Starting a project**

![2](/assignments/git_images/2.png)

3. **Local changes**

![3](/assignments/git_images/3.png)


4. **Track changes**

![4](/assignments/git_images/4.png)

5. **Commit History**

![5](/assignments/git_images/5.png)

6. **Ignoring files**

![6](/assignments/git_images/6.png)

7. **Branching**

![7](/assignments/git_images/7.png)

8. **Merging**

![8](/assignments/git_images/8.png)

9. **Remote**

![9](/assignments/git_images/9.png)

10. **Pushing updates**

![10](/assignments/git_images/10.png)

11. **Pulling updates**

![11](/assignments/git_images/11.png)

12. **Undo changes**

![12](/assignments/git_images/12.png)

13. **Removing files**

![13](/assignments/git_images/13.png)

> This is all from my side , for more information go through the [link](https://www.javatpoint.com/git)
> <div> :+1: :wave: </div>
