# GitLab

* GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager.
* It provides wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc.
* It follows an open-core development model where the core functionality is released under an open-source (MIT) license while the additional functionality is under a proprietary license.

> This all from my side , for more information go through [wikipedia](https://en.wikipedia.org/wiki/GitLab7).
>
>:+1:
